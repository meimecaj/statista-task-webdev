window.onload = () => {
  let ul = document.getElementById("fizz-buzz-list");
  ul.className = "list";

  for (let i = 1; i <= 100; i++) {
    let element = document.createElement("li");
    element.className = "list__elem";

    if (i % 3 === 0 || i % 5 === 0) {
      if (i % 3 === 0) {
        element.innerText += "Fizz";
        element.className += " list__elem--highlight";
      }
      if (i % 5 === 0) {
        element.innerText += "Buzz";
        element.className += " list__elem--highlight"
      }
    } else {
      element.innerText = i;
    }

    ul.appendChild(element);
  }
};
